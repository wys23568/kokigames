var SG_ImagePath = './images/';
var SG_Game_Started = false;

MY_START_GAME = function() {
    var lang = SG.lang;
    SG_ImagePath += lang+'/';
    globalInit();
    console.log("MY_START_GAME");
}

MY_MORE_GAMES = function() {
    console.log("MY_MORE_GAMES");
    document.location = MORE_URL;
};

MY_MAINMENU = function() {
    console.log("MY_MAINMENU");
};


MY_GAMEPLAY = function() {
    if (!SG_Game_Started) { SG_Game_Started = true; SG_Hooks.start(); }
    console.log("MY_GAMEPLAY");
}


MY_GAMECOMPLETE = function() {
    console.log("MY_GAMECOMPLETE");
};

MY_SUBMITSCORE = function(score) {
    console.log("MY_SUBMITSCORE", score);
}
