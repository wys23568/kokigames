// ===============================================
// .GEARS Studios - Game Code for Droplet Shuffle
// 18-02-2013
// Version 1.0.0
// ===============================================

SG.detectPortalUrl();
var MORE_URL = SG.portalURL;

//must have
var m_imageList = new Array("brand_logo", "load_bar", "load_element", "number_context", "number_score", "medals", "blink", "brand_copyright", "new", "buttons", "title", "cup", "droplet", "hand", "text", "score_panel", "bg");

var m_button_ok;
var m_button_start;
var m_button_more;
var m_button_submit;
var m_isMenuState;
var m_cups;
var m_droplet;
var m_foundCount;
var m_hands;
var m_liftTime;
var m_score;
var m_scoreFont;
var m_levelFont;
var m_textReady;
var m_textGameOver;
var m_state;
var m_shuffleControl;
var m_isOpenTime = false;
var m_needScoreDisplay = false;
var m_scorePanel;

function TextReady()
{
    this.alpha = 0;
    this.x = 0;
    this.y = 0;
    this.active = false;
    this.visible = false;
    this.tween = new dot_Tween();
    this.alphaTween = new dot_Tween();
    this.state = 0;
    this.self = this;
    
    this.reset = function()
    {
        m_textReady.active = true;
        m_textReady.visible = true;
        m_textReady.x = -img_text.width;
        m_textReady.y = (SCREEN_HEIGHT >> 1) - 50;
        m_textReady.tween.reset(m_textReady.x, ((SCREEN_WIDTH - img_text.width) >> 1), EASE_QUAD_INOUT, 0.5);
        m_textReady.alphaTween.reset(0, 1, EASE_QUAD_INOUT, 0.5);
        m_textReady.state = 0;
        m_textReady.alpha = 0;
    }
    
    this.update = function(Elapsed)
    {
        switch (this.state)
        {
            case 0:
            {
                this.tween.update(Elapsed);
                this.alphaTween.update(Elapsed);
                this.x = this.tween.delta;
                this.alpha = this.alphaTween.delta;
                if (this.tween.finished)
                {
                    this.tween.reset(0, 1, EASE_QUAD_INOUT, 2.5);
                    this.state = 1;
                }
                break;
            }
                
            case 1:
            {
                this.tween.update(Elapsed);
                if (this.tween.finished)
                {
                    this.state = 2;
                    this.tween.reset(this.x, SCREEN_WIDTH, EASE_QUAD_INOUT, 0.5);
                    this.alphaTween.reset(1, 0, EASE_QUAD_INOUT, 0.5);
                }
                break;
            }
                
            case 2:
            {
                this.tween.update(Elapsed);
                this.alphaTween.update(Elapsed);
                this.x = this.tween.delta;
                this.alpha = this.alphaTween.delta;
                if (this.tween.finished)
                {
                    this.state = 3;
                    this.active = false;
                    this.visible = false;
                    m_needScoreDisplay = true;
                }
                break;
            }
        }
    }
    
    this.render = function(Context, OffsetX, OffsetY)
    {
        Context.globalAlpha = this.alpha;
        Context.drawImage(img_text, 0, 0, img_text.width, 10, Math.floor(this.x), Math.floor(this.y), img_text.width, 10);
        Context.globalAlpha = 1;
    }
}

function TextGameOver()
{
    this.alpha = 0;
    this.x = 0;
    this.y = 0;
    this.active = false;
    this.visible = false;
    this.alphaTween = new dot_Tween();
    this.state = 0;
    this.self = this;
    this.speed_y = 0;
    this.accel_y = 0;
    
    this.reset = function()
    {
        m_textGameOver.active = true;
        m_textGameOver.visible = true;
        m_textGameOver.x = ((SCREEN_WIDTH - img_text.width) >> 1);
        m_textGameOver.y = (SCREEN_HEIGHT >> 1) - 50;
        m_textGameOver.alphaTween.reset(0, 1, EASE_QUAD_INOUT, 0.5);
        m_textGameOver.state = 0;
        m_textGameOver.alpha = 0;
        m_textGameOver.speed_y = -2;
        m_textGameOver.accel_y = 0.5;
        m_needScoreDisplay = false;
    }
    
    this.update = function(Elapsed)
    {
        switch (this.state)
        {
            case 0:
            {
                this.alphaTween.update(Elapsed);
                this.alpha = this.alphaTween.delta;
                this.y += this.speed_y;
                this.speed_y += this.accel_y;
                if (this.y >= (SCREEN_HEIGHT >> 1) - 50)
                {
                    this.y = (SCREEN_HEIGHT >> 1) - 50;
                    this.speed_y = 0;
                    this.accel_y = 0;
                }
                
                if (this.alphaTween.finished)
                {
                    this.alphaTween.reset(0, 1, EASE_QUAD_INOUT, 1);
                    this.state = 1;
                }
                break;
            }
                
            case 1:
            {
                this.alphaTween.update(Elapsed);
                if (this.alphaTween.finished)
                {
                    this.state = 2;
                    this.alpha = 1;
                    this.alphaTween.reset(this.y, this.y - 5, EASE_QUAD_INOUT, 0.25);
                    m_scorePanel.reset(m_score);
                }
                break;
            }
                
            case 2:
            {
                this.alphaTween.update(Elapsed);
                this.y = this.alphaTween.delta;
                if (this.alphaTween.finished)
                {
                    this.state = 3;
                    MY_GAMECOMPLETE();
                    SG_Hooks.gameOver(m_round,m_score);
                    xmlHttp = createXMLHttpRequest();
        var sco = m_score;
        var params = "?game_id=37&score=" + sco;
    var url = '//' + window.location.host + "/wechat/game/score" + params;//"https://test.i.yangcongtao.com/wechat/isLogin"; 
    // host = window.location.host;
    // host2=document.domain; 
    // var url2 = host + "/test/iosaccount.php";
    xmlHttp.open("GET", url, true);// 异步处理返回   
    xmlHttp.onreadystatechange = function()
  {
  if (xmlHttp.readyState==4 && xmlHttp.status==200)
    {
    console("test is %s",xmlHttp.responseText);
    }
  };   
    xmlHttp.setRequestHeader("Content-Type",  
        "application/json;"); 
    xmlHttp.setRequestHeader("Accept","application/json"); 
    xmlHttp.send();

    function createXMLHttpRequest() {  
    var xmlHttp;  
    if (window.XMLHttpRequest) {  
        xmlHttp = new XMLHttpRequest();  
        if (xmlHttp.overrideMimeType)  
            xmlHttp.overrideMimeType('text/xml');  
    } else if (window.ActiveXObject) {  
        try {  
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");  
        } catch (e) {  
            try {  
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");  
            } catch (e) {  
            }  
        }  
    }  
    return xmlHttp;  
};
                }
                break;
            }
                
            case 3:
            {
                break;
            }
        }
    }
    
    this.render = function(Context, OffsetX, OffsetY)
    {
        Context.globalAlpha = this.alpha;
        Context.drawImage(img_text, 0, 10, img_text.width, 10, Math.floor(this.x), Math.floor(this.y), img_text.width, 10);
        Context.globalAlpha = 1;
    }
}

function HandControl()
{
    this.tween = new dot_Tween();
    this.start_x = 0;
    this.start_y = 0;
    this.end_x = 0;
    this.end_y = 0;
    this.range_x = 0;
    this.range_y = 0;
    this.frontHand = null;
    this.backHand = null;
    this.state = 0;
    
    this.reset = function(StartX, StartY, EndX, EndY, FrontCup, BackCup)
    {
        this.start_x = StartX;
        this.start_y = StartY;
        this.end_x = EndX;
        this.end_y = EndY;
        this.range_x = this.end_x - this.start_x;
        this.range_y = this.end_y - this.start_y;
        this.frontHand = FrontCup;
        this.backHand = BackCup;
        if (this.start_x < this.end_x)
        {
            this.frontHand.handId = 1;
            this.backHand.handId = 2;
        }
        else
        {
            this.frontHand.handId = 2;
            this.backHand.handId = 1;
        }
        this.active = true;
        this.state = 1;
        this.tween.reset(0, 1, EASE_QUAD_INOUT, 0.25);
    }
    
    this.update = function(Elapsed)
    {
        switch (this.state)
        {
            case 1:
            {
                if (!this.tween.finished)
                {
                    this.tween.update(Elapsed);
                    this.frontHand.handAlpha = this.tween.delta;
                    this.backHand.handAlpha = this.tween.delta;
                }
                else
                {
                    this.frontHand.handAlpha = 1;
                    this.backHand.handAlpha = 1;
                    this.tween.reset(0, 180, EASE_QUAD_INOUT, Math.max((1.5 - m_shuffleControl.shuffleCount * 0.1), 0.25));
                    this.state = 2;
                }
                break;
            }
                
            case 2:
            {
                if (!this.tween.finished)
                {
                    this.tween.update(Elapsed);
                    var _dx = m_math.cos(Math.floor(this.tween.delta)) * this.range_x * 0.5;
                    this.frontHand.x = this.start_x + this.range_x * 0.5 - _dx;
                    this.backHand.x = this.start_x + this.range_x * 0.5 + _dx;
                    var _dy = m_math.sin(Math.floor(this.tween.delta)) * this.range_x * 0.5;
                    if (this.start_x < this.end_x)
                    {
                        this.frontHand.y = this.start_y + _dy;
                        this.backHand.y = this.end_y - _dy;
                    }
                    else
                    {
                        this.frontHand.y = this.start_y - _dy;
                        this.backHand.y = this.end_y + _dy;
                    }
                }
                else
                {
                    this.frontHand.x = this.end_x;
                    this.frontHand.y = this.end_y;
                    this.backHand.x = this.start_x;
                    this.backHand.y = this.start_y;
                    this.tween.reset(1, 0, EASE_QUAD_INOUT, 0.25);
                    this.state = 3;
                }
                break;
            }
                
            case 3:
            {
                if (!this.tween.finished)
                {
                    this.tween.update(Elapsed);
                    this.frontHand.handAlpha = this.tween.delta;
                    this.backHand.handAlpha = this.tween.delta;
                }
                else
                {
                    this.frontHand.handAlpha = 0;
                    this.backHand.handAlpha = 0;
                    this.frontHand.handId = 0;
                    this.backHand.handId = 0;
                    this.frontHand = null;
                    this.backHand = null;
                    this.active = false;
                }
                break;
            }
        }
        
    }
}

function ShuffleControl()
{
    this.handControl = null;
    this.shuffleCount = 0;
    this.currentCount = 0;
    this.step = 0;
    this.active = false;
    this.indexes = new Array(4);
    m_round = 0;

    this.reset = function()
    {
        m_math.randomize();
        this.currentCount = 0;
        this.active = true;
        this.handControl = m_hands;
        this.indexes[0] = 0;
        this.indexes[1] = 1;
        this.indexes[2] = 2;
        this.indexes[3] = 3;
        this.shuffleCount = 1;
        this.step = 0;
        m_liftTime = 0;
        m_round = this.shuffleCount;
    }
    
    this.next = function()
    {
        if (this.step >= 1)
        {            
            this.shuffleCount++;
            m_round = this.shuffleCount;
            this.step = 0;
        }
        this.currentCount = 0;
        this.active = true;
        this.step++;
        m_liftTime = 0;
    }
    
    this.last_c0 = -1;
    this.last_c1 = -1;
    this.c0 = -1;
    this.c1 = -1;
    
    this.update = function(Elapsed)
    {
        if (this.currentCount < this.shuffleCount)
        {
            if (!this.handControl.active)
            {
                this.last_c0 = this.c0;
                this.last_c1 = this.c1;
                this.c0 = 0;
                this.c1 = 0;
                var i = 0;
                while (i < 5)
                {
                    if (this.shuffleCount > 5)
                    {
                        if (m_math.rand() % 2 == 0)
                        {
                            if (m_math.rand() % 2 == 0)
                            {
                                this.c0 = m_math.randomIn(0, 3);
                                this.c1 = this.c0 + 1;
                            }
                            else
                            {
                                this.c0 = m_math.randomIn(1, 3);
                                this.c1 = this.c0 - 1;
                            }
                        }
                        else
                        {
                            if (m_math.rand() % 2 == 0)
                            {
                                this.c0 = m_math.randomIn(0, 2);
                                this.c1 = this.c0 + 2;
                            }
                            else
                            {
                                this.c0 = m_math.randomIn(2, 2);
                                this.c1 = this.c0 - 2;
                            }
                        }
                    }
                    else
                    {
                        if (m_math.rand() % 2 == 0)
                        {
                            this.c0 = m_math.randomIn(0, 3);
                            this.c1 = this.c0 + 1;
                        }
                        else
                        {
                            this.c0 = m_math.randomIn(1, 3);
                            this.c1 = this.c0 - 1;
                        }
                    }
                    i++;
                    if (this.c0 + this.c1 != this.last_c0 + this.last_c1)
                        break;
                }
                
                var i0 = this.indexes[this.c0];
                var i1 = this.indexes[this.c1];
                
                var tg = this.indexes[this.c0];
                this.indexes[this.c0] = this.indexes[this.c1];
                this.indexes[this.c1] = tg;
                
                this.handControl.reset(m_cups.array[i0].x, m_cups.array[i0].y, m_cups.array[i1].x, m_cups.array[i1].y, m_cups.array[i0], m_cups.array[i1]);
            }
            else
            {
                this.handControl.update(Elapsed);
                if (!this.handControl.active)
                {
                    this.currentCount++;
                    if (this.currentCount == this.shuffleCount)
                        m_liftTime = 2;
                }
            }
        }
        else
        {
        }
    }
}

function Droplet()
{
    this.load(img_droplet, 12, 12, 0, 0, 12, 12, true);
    this.smile = this.createAnimation("smile", 4, true, [0, 1]);
    this.idle = this.createAnimation("idle", 1, false, [0]);
    
    this.reset = function(X, Y)
    {
        this.x = Math.floor(X);
        this.y = Math.floor(Y);
        this.active = true;
        this.visible = false;
        this.idle.play(true);
    }
    
    this.update = function(Elapsed)
    {
        this.updateAnimation(Elapsed);
    }
}

function Cup()
{
    this.x = 0;
    this.y = 0;
    this.active = false;
    this.visible = false;
    this.tween = new dot_Tween();
    this.moveId = 0;
    this.z = 0;
    this.justPressed = false;
    this.justReleased = false;
    this.isPressed = false;
    this.currentFrame = 0;
    this.w = 16;
    this.h = 18;
    this.dropletId = 0;
    this.handId = 0;
    this.handAlpha = 0;
    
    var self = this;

    this.reset = function(X, Y, DropletId)
    {
        this.x = Math.floor(X);
        this.y = Math.floor(Y);
        this.z = 0;
        this.handId = 0;
        this.handAlpha = 0;
        this.active = true;
        this.visible = true;
        this.moveId = 0;
        
        this.pressing = false;
        
        this.justReleased = false;
        this.justPressed = false;
        this.isPressed = false;
        this.currentFrame = 0;
        
        this.dropletId = 0;
        
        this.release();
    }
    
    this.press = function()
    {
        this.justReleased = false;
        if (!this.isPressed)
            this.justPressed = true;
        
        this.isPressed = true;
    }
    
    this.release = function()
    {
        this.justPressed = false;
        if (this.isPressed)
            this.justReleased = true;
        
        this.isPressed = false;
    }
    
    this.nextLevel = function()
    {
        if (!m_shuffleControl.active)
            m_shuffleControl.reset();
        else {
            m_paused = true;
            SG_Hooks.levelUp(m_round+1,m_score, function() { m_paused = false; } );
        }
        
        m_shuffleControl.next();
    }
    
    this.closeAllLid = function()
    {
        for (var i = 0; i < 4; i++)
        {
            if (m_cups.array[i].z < 0)
                m_cups.array[i].close();
        }
        
        m_foundCount = 0;
        m_droplet.idle.play(true);
        m_droplet.currentAnim = m_droplet.idle;
        
        m_time.addTimeOut(1, self.nextLevel);
    }
    
    this.cheer = function()
    {
        if (m_cups.array[1].z < 0 && m_cups.array[2].z < 0)
        {
            m_droplet.smile.play(true);
            m_droplet.currentAnim = m_droplet.smile;
            
            if (m_cups.array[1].z < 0)
            {
                m_scoreContextPool.next().reset(m_cups.array[1].x + m_cups.array[1].w * 0.5, m_cups.array[1].y + 8, 10, true);
                m_score += 10;
            }
            if (m_cups.array[2].z < 0)
            {
                m_scoreContextPool.next().reset(m_cups.array[2].x + m_cups.array[2].w * 0.5, m_cups.array[2].y + 8, 10, true);
                m_score += 10;
            }
            m_time.addTimeOut(3, self.closeAllLid);
        }
        else
        {
            //game over
            m_time.addTimeOut(0.5, m_textGameOver.reset);
        }
    }
    
    this.update = function(Elapsed)
    {
        this.justPressed = false;
        this.justReleased = false;
        
        if (this.pressing != this.isPressed)
        {
            if (this.isPressed)
                this.release();
            else
                this.press();
        }
        
        if (!this.tween.finished)
        {
            this.tween.update(Elapsed);
            if (this.moveId == 1 || this.moveId == 2)
            {
                this.z = this.tween.delta;
            }
        }
        else
        {
            if (this.justPressed && m_liftTime > 0)
            {
                if (this.z >= 0)
                {
                    this.lift();
                    
                    if (this.dropletId > 0)
                    {
                        m_foundCount++;
                        if (m_foundCount == 2)
                        {
                            m_time.addTimeOut(1, self.cheer);
                        }
                        m_liftTime--;
                    }
                    else
                    {
                        m_liftTime = 0;
                        m_time.addTimeOut(1, self.cheer);
                    }
                }
            }
        }
    }
    
    this.gameBegin = function()
    {
        self.closeAllLid();
    }
    
    this.lift = function()
    {
        this.moveId = 1;
        this.tween.reset(0, -11, EASE_CUBE_INOUT, 0.5);
        this.z = 0;
    }
    
    this.close = function()
    {
        this.moveId = 2;
        this.tween.reset(-11, 0, EASE_CUBE_INOUT, 0.5);
        this.z = -11;
    }
    
    this.render = function(Context, OffsetX, OffsetY)
    {
        var xx = Math.floor(this.x);
        var yy = Math.floor(this.y);
        var zz = Math.floor(this.z);
        
//        var xx = this.x;
//        var yy = this.y;
//        var zz = this.z;

        if (zz != 0)
        {
            Context.globalAlpha = 1 - (Math.abs(this.z) / 11) * 0.5;
            Context.drawImage(img_cup, 0, 18, 16, 9, xx, yy + 11, 16, 9);
            Context.globalAlpha = 1;
            if (this.dropletId > 0)
            {
                if (this.dropletId == 2)
                    m_droplet.flip = true;
                else
                    m_droplet.flip = false;
                m_droplet.render(Context, this.x + 2, this.y + 7);
            }
        }
        else
        {
            Context.drawImage(img_cup, 0, 18, 16, 9, xx, yy + 11, 16, 9);
        }
        Context.drawImage(img_cup, 0, 0, 16, 18, xx, yy + zz, 16, 18);
        
        if (this.handId > 0)
        {
            Context.globalAlpha = this.handAlpha;
            if (this.handId == 1)
            {
                Context.drawImage(img_hand, 0, 0, 13, 16, xx + 2, yy - 12, 13, 16);
            }
            else
            {
                Context.drawImage(img_hand, 13, 0, 13, 16, xx + 2, yy - 12, 13, 16);
            }
            Context.globalAlpha = 1;
        }
    }
}

this.gameUpdate = function(Elapsed, Context)
{
    Context.drawImage(img_bg, 0, 0);
    if (m_isMenuState)
    {
        Context.drawImage(img_title, 38, 61);
        //Context.drawImage(img_brand_copyright, (SCREEN_WIDTH - img_brand_copyright.width) >> 1, SCREEN_HEIGHT - 15 - img_brand_copyright.height);
    }
    else
    {
        switch (m_state)
        {
            case 0:
            {
                m_droplet.update(Elapsed);
                m_cups.update(Elapsed);
                
                if (m_shuffleControl.active)
                    m_shuffleControl.update(Elapsed);
                
                if (m_hands.active)
                {
                    if (m_hands.backHand)
                        m_hands.backHand.render(Context, 0, 0);
                }
                
                for (var i = 0; i < 4; i++)
                {
                    if (m_cups.array[i].handId == 0)
                        m_cups.array[i].render(Context, 0, 0);
                }
                
                if (m_hands.frontHand)
                {
                    m_hands.frontHand.render(Context, 0, 0);
                }
                
                if (m_needScoreDisplay)
                    m_scoreFont.writeCentered(Context, m_score, SCREEN_WIDTH >> 1, 10, false, 12);
                
                if (m_textReady.active)
                {
                    m_textReady.update(Elapsed);
                    m_textReady.render(Context, 0, 0);
                }
                
                if (m_textGameOver.active)
                {
                    m_textGameOver.update(Elapsed);
                    m_textGameOver.render(Context, 0, 0);
                }
                
                if (m_scorePanel.active)
                {
                    m_scorePanel.update(Elapsed);
                    m_scorePanel.render(Context, 0, 0);
                    
                    if (m_scorePanel.state == 2 && !m_button_ok.active)
                    {
//                        m_button_submit.reset(((SCREEN_WIDTH - (m_button_submit.w * 2) - 10) >> 1), (SCREEN_HEIGHT >> 1) + (m_scorePanel.h >> 1) + 10);
//                        m_button_submit.visible = true;
                        
                        m_button_ok.reset((SCREEN_WIDTH - m_button_ok.w) >> 1, 150);
                        m_button_ok.visible = true;
                    }
                }
                break;
            }
        }
    }
    
    if (m_button_start.active)
    {
        m_button_start.update(Elapsed);
        m_button_start.render(Context, 0, 0);
        
        if (m_button_start.justReleased && !m_effect.active)
            m_effect.reset(EFFECT_FADE_IN, 0.5, 1);
    }
    
    if (m_button_ok.active)
    {
        m_button_ok.update(Elapsed);
        m_button_ok.render(Context, 0, 0);
        
        if (m_button_ok.justReleased && !m_effect.active)
            m_effect.reset(EFFECT_FADE_IN, 0.5, 3);
    }

    if (m_button_more.active)
    {
        m_button_more.update(Elapsed);
        m_button_more.render(Context, 0, 0);
        
        if (m_button_more.justReleased && !m_effect.active)
            MY_MORE_GAMES();
    }
    
    if (m_button_submit.active)
    {
        m_button_submit.update(Elapsed);
        m_button_submit.render(Context, 0, 0);
        
        if (m_button_submit.justReleased && !m_effect.active)
        {
            MY_SUBMITSCORE(m_score);
            m_effect.reset(EFFECT_FADE_IN, 0.5, 3);
        }
    }
    
    if (m_effect.finished)
    {
        if (m_effect.info == 1)
        {
            m_effect.info = -1;
            m_button_start.active = false;
            m_button_start.visible = false;
            
            m_button_more.active = false;
            m_button_more.visible = false;
            
            m_time.addTimeOut(4, m_cups.array[0].gameBegin);
            m_time.addTimeOut(0.5, m_textReady.reset);
            
            m_isMenuState = false;
            m_effect.reset(EFFECT_FADE_OUT, 0.5, 2);
            MY_GAMEPLAY();
        }
        else if (m_effect.info == 3)
        {
            m_effect.info = -1;
            gameReset();
            m_effect.reset(EFFECT_FADE_OUT, 0.5, 4);
            MY_MAINMENU();
        }
    }
}

this.gameInit = function()
{
    Droplet.prototype = new dot_Sprite();
    Droplet.prototype.constructor = Droplet;
    
    m_button_start = new dot_Button();
    m_button_start.create(img_buttons, 0, 13 * 2, 40, 13);
    
    m_button_ok = new dot_Button();
    m_button_ok.create(img_buttons, 0, 13, 40, 13);
    
    m_button_more = new dot_Button();
    m_button_more.create(img_buttons, 0, 13 * 3, 40, 13);
    
    m_button_submit = new dot_Button();
    m_button_submit.create(img_buttons, 0, 0, 40, 13);
    
    m_cups = new dot_ObjectPool();
    m_cups.create(Cup, 4);
    
    m_droplet = new Droplet();
    
    m_hands = new HandControl();
    
    m_scoreFont = new dot_NumberFont();
    m_scoreFont.load(img_number_score, 8, 10);
    
    m_textReady = new TextReady();
    m_textGameOver = new TextGameOver();
    
    m_shuffleControl = new ShuffleControl();
    
    m_scorePanel = new dot_ScorePanel();
    m_scorePanel.create(img_score_panel, "ShuffleBest", m_scoreFont, 200, 300, 400, 500);

    gameReset();
}

this.gameReset = function()
{
    m_ignoreTouchMove = true;
    
    m_isMenuState = true;
    
    var _mid = Math.floor((108 - img_cup.width * 4)/ 5);
    var _xx = 28;
    var _yy = Math.floor((SCREEN_HEIGHT >> 1) + 9);
    for (var i = 0; i < 4; i++)
    {
        m_cups.array[i].reset(_xx, _yy);
        _xx += img_cup.width + _mid;
    }
    
    m_cups.array[1].dropletId = 1;
    m_cups.array[2].dropletId = 2;
    
    m_shuffleControl.active = false;
    
    m_droplet.reset(0, 0);
    
    m_textGameOver.active = false;
    m_scorePanel.active = false;
    m_button_ok.active = false;
    m_button_submit.active = false;
    
    m_button_start.reset(((SCREEN_WIDTH - (m_button_start.w * 2) - 15) >> 1), 150);
    m_button_start.visible = true;
    
    m_button_more.reset(m_button_start.x + m_button_start.w + 15, 150);
    m_button_more.visible = true;
    
    m_hands.active = false;
    
    m_cups.array[0].lift();
    m_cups.array[1].lift();
    m_cups.array[2].lift();
    m_cups.array[3].lift();
    
    m_foundCount = 0;
    m_liftTime = 0;
    m_score = 0;
    m_state = 0;
    m_needScoreDisplay = false;
}

this.touchPressed = function(X, Y)
{
    if (m_cups && m_cups.array[0].active)
    {
        for (var i = 0; i < 4; i++)
        {
            if (X > m_cups.array[i].x && X < m_cups.array[i].x + m_cups.array[i].w && Y > m_cups.array[i].y && Y < m_cups.array[i].y + m_cups.array[i].h)
            {
                m_cups.array[i].pressing = true;
            }
        }
    }
    
    if (m_button_ok && m_button_ok.active)
    {
        if (X > m_button_ok.x && X < m_button_ok.x + m_button_ok.w && Y > m_button_ok.y && Y < m_button_ok.y + m_button_ok.h)
        {
            m_button_ok.pressing = true;
        }
    }
    
    if (m_button_start && m_button_start.active)
    {
        if (X > m_button_start.x && X < m_button_start.x + m_button_start.w && Y > m_button_start.y && Y < m_button_start.y + m_button_start.h)
        {
            m_button_start.pressing = true;
        }
    }
    
    if (m_button_submit && m_button_submit.active)
    {
        if (X > m_button_submit.x && X < m_button_submit.x + m_button_submit.w && Y > m_button_submit.y && Y < m_button_submit.y + m_button_submit.h)
        {
            m_button_submit.pressing = true;
        }
    }
    
    if (m_button_more && m_button_more.active)
    {
        if (X > m_button_more.x && X < m_button_more.x + m_button_more.w && Y > m_button_more.y && Y < m_button_more.y + m_button_more.h)
        {
            m_button_more.pressing = true;
        }
    }
}

this.touchReleased = function(X, Y)
{
    if (m_cups && m_cups.array[0].active)
    {
        for (var i = 0; i < 4; i++)
        {
            if (X > m_cups.array[i].x && X < m_cups.array[i].x + m_cups.array[i].w && Y > m_cups.array[i].y && Y < m_cups.array[i].y + m_cups.array[i].h)
            {
                m_cups.array[i].pressing = false;
            }
        }
    }
    
    if (m_button_ok && m_button_ok.active)
    {
        if (X > m_button_ok.x && X < m_button_ok.x + m_button_ok.w && Y > m_button_ok.y && Y < m_button_ok.y + m_button_ok.h)
        {
            m_button_ok.pressing = false;
        }
    }
    
    if (m_button_start && m_button_start.active)
    {
        if (X > m_button_start.x && X < m_button_start.x + m_button_start.w && Y > m_button_start.y && Y < m_button_start.y + m_button_start.h)
        {
            m_button_start.pressing = false;
        }
    }
    
    if (m_button_submit && m_button_submit.active)
    {
        if (X > m_button_submit.x && X < m_button_submit.x + m_button_submit.w && Y > m_button_submit.y && Y < m_button_submit.y + m_button_submit.h)
        {
            m_button_submit.pressing = false;
        }
    }
    
    if (m_button_more && m_button_more.active)
    {
        if (X > m_button_more.x && X < m_button_more.x + m_button_more.w && Y > m_button_more.y && Y < m_button_more.y + m_button_more.h)
        {
            m_button_more.pressing = false;
        }
    }
}