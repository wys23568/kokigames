// ===============================================
// .GEARS Studios - Javascript Game Engine
// 04-03-2013
// ===============================================

var ENGINE_VERSION = "2.0.0"; //not backward compatible

// v2.0.0 What's New
//  1. Screen Resolution changed to 144x208
//  2. Screen Scale with CSS Scale
//  3. Single Canvas for rendering

// v1.0.6 What's New - Original built for Droplet Shuffle
//  1. Added m_time (dot_Time) for scheduling function call
//  2. Added m_math.random() for integer random (faster)
//  3. Added dot_ScorePanel for final score display

// v1.0.5 What's New - Original built for BoosterMedia intergrated work
//  1. Screen dynamic resize
//  2. Add orientation change notify

// v1.0.4 What's New - Original built for Lonely Ball: Vertical Journey
//  1. Removed dot_Sprite.postUpdate and dotSprite.speed_x, dot_Sprite.speed_y, dot_Sprite.accel_x, dot_Sprite.accel_y
//  2. Change dot_Sprite.preUpdate to dot_Sprite.updateAnimation
//  2. Removed dot_Sprite.addAnim and added dot_Sprite.createAnimation() - This update helps reduce String allocation in game loop.
//  3. Change dot_Sprite.kill to dot_Sprite.deactive

// v1.0.3 What's New
//  1. Updated dot_VarTween: No longer using String in reset() to be compatible with obfuscate
//  2. Updated dot_Sprite.reset() added Math.floor to X, Y
//  3. Removed dot_Scene entirely
//  4. Added dot_ScoreContext to display context score.
//  5. Added effect fade in & out
//  6. Added dot_ObjectPool for pooling/reusing objects (looping queue method)
//  7. Added dot_Button for simple button mechanism
//  8. Added dot_Medal for uniform medal display

function dot_Anim(Name, Rate, Loop, Frames)
{
    var self = this;
    this.name = Name;
    this.timing = 1 / Rate;
    this.loop = Loop;
    this.frames = Frames;
    this.numberOfFrames = this.frames.length;
    
    this.currentFrameIndex = 0;
    this.timer = 0;
    this.finished = false;
    
    this.dirty = true;
    this.currentFrame = 0;
    
    this.parent = null;
    
    this.reset = function()
    {
        this.currentFrameIndex = 0;
        this.timer = 0;
        this.finished = false;
        this.dirty = true;
        this.currentFrame = this.frames[this.currentFrameIndex];
    }
    
    this.update = function(Elapsed)
    {
        if (!this.finished)
        {
            this.timer += Elapsed;
            if (this.timer > this.timing)
            {
                this.currentFrameIndex++;
                this.dirty = true;
                if (this.currentFrameIndex >= this.numberOfFrames)
                {
                    this.currentFrameIndex = 0;
                    if (!this.loop)
                        this.finished = true;
                }
                this.currentFrame = this.frames[this.currentFrameIndex];
                this.timer = 0;
                
            }
            else
                self.dirty = false;
        }
    }
    
    this.play = function(ForceReset)
    {
        if (this == this.parent.currentAnim)
        {
            if (ForceReset && this.parent.currentAnim.loop)
                this.parent.currentAnim.reset();
            else
                this.parent.currentAnim.reset();
        }
        else
        {
            self.parent.currentAnim = self;
            self.reset();
        }
    }
    
    this.stop = function()
    {
        this.finished = true;
    }
}

function dot_Sprite()
{
    this.self = this;
    
    this.load = function(Image, FrameWidth, FrameHeight, OffsetX, OffsetY, Width, Height, FlipSupport)
    {
        this.image = Image;
        this.frameWidth = FrameWidth;
        this.frameHeight = FrameHeight;
        this.offsetX = OffsetX;
        this.offsetY = OffsetY;
        this.w = Width;
        this.h = Height;
        this.visible = true;
        this.currentAnim = null;
        this.scene = null;
        this.flip = 0;
        this.frames = new Array;
        this.numberOfFrames = 0;
        
        this.alpha = 1;
        
        var countH = this.image.height / this.frameHeight;
        var countW = this.image.width / this.frameWidth;
        for (var j = 0; j < countH; j++)
        {
            for (var i = 0; i < countW; i++)
            {
                this.frames[this.numberOfFrames] = {
                x: i * FrameWidth,
                y: j * FrameHeight
                };
                this.numberOfFrames++;
            }
        }
        
        if (FlipSupport)
            this.numberOfFrames /= 2;
    }
    
    this.createAnimation = function(Name, Rate, Loop, Frames)
    {
        var anim = new dot_Anim(Name, Rate, Loop, Frames);
        anim.parent = self;
        this.currentAnim = anim;
        this.currentAnim.reset();
        return anim;
    }
        
    this.render = function(Context, OffsetX, OffsetY)
    {
        var currentFrame = this.currentAnim.currentFrame + this.flip * this.numberOfFrames;
        
        var xx = Math.round(this.x - this.offsetX + OffsetX);
        var yy = Math.round(this.y - this.offsetY + OffsetY);
        
        if (this.alpha != 1)
            Context.globalAlpha = this.alpha;
        
        Context.drawImage(this.image, this.frames[currentFrame].x, this.frames[currentFrame].y, this.frameWidth, this.frameHeight, xx, yy, this.frameWidth, this.frameHeight);
        
        Context.globalAlpha = 1;
    }
    
    this.reset = function(X, Y)
    {
        this.x = Math.floor(X);
        this.y = Math.floor(Y);
        this.active = true;
        this.visible = true;
    }
    
    this.deactive = function()
    {
        this.active = false;
        this.visible = false;
    }
    
    this.update = function(Elapsed)
    {
    }
    
    this.updateAnimation = function(Elapsed)
    {
        if (this.currentAnim)
            this.currentAnim.update(Elapsed);
    }
}

function dot_Font()
{
    this.load = function(Image, CharList, CharWidths, CharHeight, LineSpacing, MultitplySize, HasTwoColor)
    {
        this.image = Image;
        this.charOffsetX = new Object();
        this.charOffsetY = new Object();
        this.charHeight = CharHeight * MultitplySize;
        this.lineSpacing = LineSpacing * MultitplySize;
        this.multiplySize = MultitplySize;
        
        var xx = 0;
        var yy = 0;
        var index = 0;
        
        this.charWidths = new Object();
        
        var c;
        for (var i = 0; i < CharList.length; i++)
        {
            c = CharList[i];
            if (c === '\n')
            {
                xx = 0;
                yy += this.charHeight;
            }
            else
            {
                this.charOffsetX[c] = xx;
                this.charOffsetY[c] = yy;
                this.charWidths[c] = (CharWidths[index] + 1) * this.multiplySize;
                xx += this.charWidths[c];
                index++;
            }
        }
        
        if (HasTwoColor)
            this.offsetBlackWhiteY = Math.round(this.image.height / 2);
        else
            this.offsetBlackWhiteY = 0;
    }
    
    this.write = function(Context, X, Y, Text, IsWhite, StartIndex, CharCount)
    {
        var u = X;
        var v = Y;
        
        var xx, yy;
        var c;
        for (var i = StartIndex; i < CharCount; i++)
        {
            c = Text[i];
            xx = this.charOffsetX[c];
            yy = this.charOffsetY[c];
            
            
            if (xx === undefined || yy === undefined)
            {
                u += this.charWidths[' '];
                if (c === '\n')
                {
                    u = X;
                    v += this.lineSpacing;
                }
            }
            else
            {
                if (!IsWhite)
                    Context.drawImage(this.image, xx, yy, this.charWidths[c], this.charHeight, u, v, this.charWidths[c], this.charHeight);
                else
                    Context.drawImage(this.image, xx, yy + this.offsetBlackWhiteY, this.charWidths[c], this.charHeight, u, v, this.charWidths[c], this.charHeight);
                
                u += this.charWidths[c];
            }
        }
    }
    
    this.getTextSize = function(Text)
    {
        var u = 0;
        var v = 0;
        var maxU = 0;
        
        var xx, yy;
        var c;
        for (var i = 0; i < Text.length; i++)
        {
            c = Text[i];
            xx = this.charOffsetX[c];
            yy = this.charOffsetY[c];
            if (xx === undefined || yy === undefined)
            {
                u += this.charWidths[' '];;
                if (c === '\n')
                {
                    if (maxU < u)
                        maxU = u;
                    
                    u = 0;
                    v += this.lineSpacing;
                }
            }
            else
            {
                u += this.charWidths[c];
            }
        }
        
        if (maxU < u)
            maxU = u;
        
        return { w: maxU, h: (v + this.charHeight) };
    }
}

function dot_NumberFont()
{
    this.load = function(Image, CharWidth, CharHeight)
    {
        this.image = Image;
        this.charWidth = CharWidth;
        this.charHeight = CharHeight;
    }
    
    this.write = function(Context, SomeNumber, X, Y, ZeroFilled, Length)
    {
        var x = X - this.charWidth;
        var aNumber = 0;
        var zeroFilled = true;
        while (Length > 0)
        {
            if (SomeNumber > 0 || zeroFilled)
            {
                aNumber = SomeNumber % 10;
                Context.drawImage(this.image, this.charWidth * aNumber, 0, this.charWidth, this.charHeight, x, Y, this.charWidth, this.charHeight);
                SomeNumber = Math.floor(SomeNumber / 10);
                x -= this.charWidth;
                zeroFilled = ZeroFilled;
            }
            Length--;
        }
    }
    
    this.writeCentered = function(Context, SomeNumber, X, Y, ZeroFilled, Length)
    {
        var ww = 0;
        var aNumber = 0;
        var zeroFilled = true;
        var _length = Length;
        var _someNumber = SomeNumber;
        while (_length > 0)
        {
            if (_someNumber > 0 || zeroFilled)
            {
                aNumber = _someNumber % 10;
                _someNumber = Math.floor(_someNumber / 10);
                ww += this.charWidth;
                zeroFilled = ZeroFilled;
            }
            _length--;
        }
        
        
        _length = Length;
        zeroFilled = true;
        var x = Math.floor(X + ww * 0.5) - this.charWidth;
        _someNumber = SomeNumber;
        while (_length > 0)
        {
            if (_someNumber > 0 || zeroFilled)
            {
                aNumber = _someNumber % 10;
                Context.drawImage(this.image, this.charWidth * aNumber, 0, this.charWidth, this.charHeight, x, Y, this.charWidth, this.charHeight);
                _someNumber = Math.floor(_someNumber / 10);
                x -= this.charWidth;
                zeroFilled = ZeroFilled;
            }
            _length--;
        }
    }
}

function dot_TalkBubble()
{
    this.reset = function(Image, ImageOffsetX, ImageOffsetY, Text, Speed, X, Y, W, Font)
    {
        this.image = Image;
        this.text = "";
        this.x = X;
        this.y = Y;
        this.offsetX = ImageOffsetX;
        this.offsetY = ImageOffsetY;
        this.w = W;
        this.font = Font;
        
        var words = Text.split(" ");
        var cw = 0;
        var ww = 0;
        for (var i = 0; i < words.length; i++)
        {
            ww = Font.getTextSize(words[i]).w;
            if (cw + ww > this.w)
            {
                this.text += "\n" + words[i] + " ";
                cw = (ww + Font.charWidths[' ']);
            }
            else
            {
                this.text += words[i] + " ";
                cw += (ww + Font.charWidths[' ']);
            }
        }
        this.currentIndex = 0;
        this.timer = 0;
        this.maxTimer = 1 / Speed;
        this.finished = false;
        this.justFinished = false;
    }
    
    this.update = function(Elapsed)
    {
        this.justFinished = false;
        if (!this.finished)
        {
            this.timer += Elapsed;
            if (this.timer > this.maxTimer)
            {
                this.timer = 0;
                this.currentIndex++;
                if (this.currentIndex == this.text.length)
                {
                    this.finished = true;
                    this.justFinished = true;
                }
            }
        }
    }
    
    this.render = function(Context, OffsetX, OffsetY, IsWhite)
    {
        Context.drawImage(this.image, Math.floor(this.x + OffsetX - this.offsetX), Math.floor(this.y + OffsetY - this.offsetY));
        this.font.write(Context, Math.floor(this.x + OffsetX), Math.floor(this.y + OffsetY), this.text, IsWhite, 0, this.currentIndex);
    }
}

function dot_Math()
{
    this.load = function()
    {
        this.m_sin = new Array(360 * 4);
        this.m_cos = new Array(360 * 4);
        
        var _angle = 0;
        for (var i = 0; i < 360 * 4; i++)
        {
            this.m_sin[i] = Math.sin(_angle * PI / 180);
            this.m_cos[i] = Math.cos(_angle * PI / 180);
            _angle += 0.25;
        }
        
        this.msquare = new Array(600);
        for (var i = 0; i < this.msquare.length; i++)
        {
            this.msquare[i] = i * i;
        }
    }
    
    this.sqrt = function(X)
    {
        X = Math.floor(X);
        var xx = (X > 0) ? X : -X;
        var left = 0;
        var right = this.msquare.length - 1;
        var mid = (left + right) >> 1;
        
        while (left < right - 1)
        {
            if (this.msquare[mid] > xx)
                right = mid;
            else if (this.msquare[mid] < xx)
                left = mid;
            else
                break;
            
            mid = (left + right) >> 1;
        }
        
        return mid;
    }
    
    this.sin = function(X)
    {
        if (X < 0)
            X += 360;
        else if (X >= 360)
            X -= 360;
        
        return this.m_sin[Math.floor(X * 4)];
    }
    
    this.cos = function(X)
    {
        if (X < 0)
            X += 360;
        else if (X >= 360)
            X -= 360;
        
        return this.m_cos[Math.floor(X * 4)];
    }
    
    this.seed_w = 1234;
    this.seed_z = 5678;
    
    this.randomize = function()
    {
        var curTime = new Date().getTime();
        this.seed_w = curTime % 32000;
        this.seed_z = curTime % 65535;
    }
    
    this.rand = function()
    {
        this.seed_z = 36969 * (this.seed_z & 65535) + (this.seed_z >> 16);
        this.seed_w = 18000 * (this.seed_w & 65535) + (this.seed_w >> 16);
        return Math.abs(((this.seed_z << 16) + this.seed_w));
    }
    
    this.random = function()
    {
        return (1 / (this.rand() % 10));
    }
    
    this.randomIn = function(Lowest, Range)
    {
        if (Range > 1)
            return Lowest + (this.rand() % Range);
        else
            return Lowest + (this.random() * Range);
    }
}

var EASE_NONE = 0;
var EASE_QUAD_IN = 1;
var EASE_QUAD_OUT = 2;
var EASE_QUAD_INOUT = 3;
var EASE_CUBE_IN = 4;
var EASE_CUBE_OUT = 5;
var EASE_CUBE_INOUT = 6;
var EASE_QUART_IN = 7;
var EASE_QUART_OUT = 8;
var EASE_QUART_INOUT = 9;
var EASE_QUINT_IN = 10;
var EASE_QUINT_OUT = 11;
var EASE_QUINT_INOUT = 12;
var EASE_SIN_IN = 13;
var EASE_SIN_OUT = 14;
var EASE_SIN_INOUT = 15;
var EASE_BOUNCE_IN = 16;
var EASE_BOUNCE_OUT = 17;
var EASE_BOUNCE_INOUT = 18;
var EASE_CIRC_IN = 19;
var EASE_CIRC_OUT = 20;
var EASE_CIRC_INOUT = 21;
var EASE_EXPO_IN = 22;
var EASE_EXPO_OUT = 23;
var EASE_EXPO_INOUT = 24;
var EASE_BACK_IN = 25;
var EASE_BACK_OUT = 26;
var EASE_BACK_INOUT = 27;

var PI = 3.1415926;
var PI2 = 1.5707963;
var B1 = 0.363636363636364;
var B2 = 0.727272727272727;
var B3 = 0.545454545454545;
var B4 = 0.909090909090909;
var B5 = 0.818181818181818;
var B6 = 0.954545;

function dot_Tween()
{
    this.t = 0;
    this.duration_inverse = 0;
    this.duration = 0;
    this.functionId = EASE_NONE;
    this.timer = 0;
    this.targetLowest = 0;
    this.targetRangeNumber = 0;
    this.finished = true;
    
    //active result
    this.delta = 0;
    
    this.reset = function(RangeLowest, RangeHighest, FunctionId, Duration)
    {
        this.duration_inverse = 1 / Duration;
        this.functionId = FunctionId;
        this.timer = 0;
        this.targetLowest = RangeLowest;
        this.targetRangeNumber = RangeHighest - RangeLowest;
        this.duration = Duration;
        this.finished = false;
        this.t = 0;
        this.delta = this.targetLowest;
        this.update(0);
    }
    
    //replay last settings
    this.replay = function()
    {
        this.finished = false;
        this.timer = 0;
        this.t = 0;
        this.delta = this.targetLowest;
        this.update(0);
    }
    
    this.update = function(Elapsed)
    {
        if (this.finished) return;
        
        this.timer += Elapsed;
        
        if (this.timer > this.duration)
        {
            this.finished = true;
            this.t = 1;
            this.delta = this.targetLowest + this.targetRangeNumber;
            return;
        }
        
        this.t = this.timer * this.duration_inverse;
        
        var t = this.t;
        
        switch (this.functionId)
        {
            case EASE_QUAD_IN:
            {
                t = t * t;
                break;
            }
            case EASE_QUAD_OUT:
            {
                t = -t * (t - 2);
                break;
            }
            case EASE_QUAD_INOUT:
            {
                t = t <= 0.5 ? t * t * 2 : 1 - (--t) * t * 2;
                break;
            }
            case EASE_CUBE_IN:
            {
                t = t * t * t;
                break;
            }
            case EASE_CUBE_OUT:
            {
                t = 1 + (--t) * t * t;
                break;
            }
            case EASE_CUBE_INOUT:
            {
                t = (t <= 0.5) ? t * t * t * 4 : 1 + (--t) * t * t * 4;
                break;
            }
            case EASE_QUART_IN:
            {
                t = t * t * t * t;
                break;
            }
            case EASE_QUART_OUT:
            {
                t = 1 - (t-=1) * t * t * t;
                break;
            }
            case EASE_QUART_INOUT:
            {
                t = (t <= 0.5) ? t * t * t * t * 8 : (1 - (t = t * 2 - 2) * t * t * t) / 2 + 0.5;
                break;
            }
            case EASE_QUINT_IN:
            {
                t = t * t * t * t * t;
                break;
            }
            case EASE_QUINT_OUT:
            {
                t = (t = t - 1) * t * t * t * t + 1;
                break;
            }
            case EASE_QUINT_INOUT:
            {
                t = ((t *= 2) < 1) ? (t * t * t * t * t) / 2 : ((t -= 2) * t * t * t * t + 2) / 2;
                break;
            }
            case EASE_SIN_IN:
            {
                t = -Math.cos(PI2 * t) + 1;
                break;
            }
            case EASE_SIN_OUT:
            {
                t = Math.sin(PI2 * t);
                break;
            }
            case EASE_SIN_INOUT:
            {
                t = -Math.cos(PI * t) / 2 + .5;
                break;
            }
            case EASE_BOUNCE_IN:
            {
                t = 1 - t;
                if (t < B1)
                {
                    t = 1 - 7.5625 * t * t;
                    break;
                }
                if (t < B2)
                {
                    t = 1 - (7.5625 * (t - B3) * (t - B3) + .75);
                    break;
                }
                if (t < B4)
                {
                    t = 1 - (7.5625 * (t - B5) * (t - B5) + .9375);
                    break;
                }
                t = 1 - (7.5625 * (t - B6) * (t - B6) + .984375);
                break;
            }
            case EASE_BOUNCE_OUT:
            {
                if (t < B1)
                {
                    t = 7.5625 * t * t;
                    break;
                }
                if (t < B2)
                {
                    t = 7.5625 * (t - B3) * (t - B3) + .75;
                    break;
                }
                if (t < B4)
                {
                    t = 7.5625 * (t - B5) * (t - B5) + .9375;
                    break;
                }
                t = 7.5625 * (t - B6) * (t - B6) + .984375;
                break;
            }
            case EASE_BOUNCE_INOUT:
            {
                if (t < .5)
                {
                    t = 1 - t * 2;
                    if (t < B1)
                    {
                        t = (1 - 7.5625 * t * t) / 2;
                        break;
                    }
                    if (t < B2)
                    {
                        t = (1 - (7.5625 * (t - B3) * (t - B3) + .75)) / 2;
                        break;
                    }
                    if (t < B4)
                    {
                        t = (1 - (7.5625 * (t - B5) * (t - B5) + .9375)) / 2;
                        break;
                    }
                    t = (1 - (7.5625 * (t - B6) * (t - B6) + .984375)) / 2;
                    break;
                }
                t = t * 2 - 1;
                if (t < B1)
                {
                    t = (7.5625 * t * t) / 2 + .5;
                    break;
                }
                if (t < B2)
                {
                    t = (7.5625 * (t - B3) * (t - B3) + .75) / 2 + .5;
                    break;
                }
                if (t < B4)
                {
                    t = (7.5625 * (t - B5) * (t - B5) + .9375) / 2 + .5;
                    break;
                }
                t = (7.5625 * (t - B6) * (t - B6) + .984375) / 2 + .5;
                break;
            }
            case EASE_CIRC_IN:
            {
                t = -(Math.sqrt(1 - t * t) - 1);
                break;
            }
            case EASE_CIRC_OUT:
            {
                t = Math.sqrt(1 - (t - 1) * (t - 1));
                break;
            }
            case EASE_CIRC_INOUT:
            {
                t = t <= .5 ? (Math.sqrt(1 - t * t * 4) - 1) / -2 : (Math.sqrt(1 - (t * 2 - 2) * (t * 2 - 2)) + 1) / 2;
                break;
            }
            case EASE_EXPO_IN:
            {
                t = Math.pow(2, 10 * (t - 1));
                break;
            }
            case EASE_EXPO_OUT:
            {
                t = -Math.pow(2, -10 * t) + 1;
                break;
            }
            case EASE_EXPO_INOUT:
            {
                t = t < .5 ? Math.pow(2, 10 * (t * 2 - 1)) / 2 : (-Math.pow(2, -10 * (t * 2 - 1)) + 2) / 2;
                break;
            }
            case EASE_BACK_IN:
            {
                t = t * t * (2.70158 * t - 1.70158);
                break;
            }
            case EASE_BACK_OUT:
            {
                t = 1 - (--t) * (t) * (-2.70158 * t - 1.70158);
                break;
            }
            case EASE_BACK_INOUT:
            {
                t *= 2;
                if (t < 1)
                {
                    t = t * t * (2.70158 * t - 1.70158) / 2;
                    break;
                }
                t--;
                t = (1 - (--t) * (t) * (-2.70158 * t - 1.70158)) / 2 + .5;
                break;
            }
        }
        
        this.t = t;
        this.delta = this.targetLowest + this.targetRangeNumber * this.t;
    }
}

var SCREEN_WIDTH = 0;
var SCREEN_HEIGHT = 0;

var BACKGROUND_COLOR = '#1d1d1d';

var m_elapsed;
var m_lastTime = 0;
var m_resourceNumber = 100;
var m_resourceCounter = 0;
var m_initialized = false;
var m_isReqAnimationEnabled;
var _reqAnimation;

var m_canvas;
var m_context;
var m_offscreen_canvas;

var m_touch_x;
var m_touch_y;
var m_touch_pressed;

var m_time;

var m_screenRenew;
var m_paused;

var m_loaderRenderEnabled = 0;
var m_effect;
var m_scoreContextPool;
var m_heartBeat; //very useful
var m_math; //sin + cos
var m_blinkPool;
var m_ignoreTouchMove;

function dot_ScoreContext()
{
    this.fontImage = img_number_context;
    this.charWidth = 6;
    this.charHeight = 7;
    this.needPlusSign = false;
    this.timer = 0;
    this.score = 0;
    this.numberOfChars = 1;
    this.speedx = 0;
    this.speedy = 0;
    this.accely = 0;
    this.accelx = 0;
    
    this.reset = function(X, Y, Score, NeedPlusSign)
    {
        this.numberOfChars = 1;
        if (Score > 999)
            this.numberOfChars = 4;
        else if (Score > 99)
            this.numberOfChars = 3;
        else if (Score > 9)
            this.numberOfChars = 2;
        
        this.needPlusSign = NeedPlusSign;
        
        this.score = Score;
        
        this.w = this.getWidth(this.score, false, this.numberOfChars) + 1;
        this.h = this.charHeight;
        if (this.needPlusSign)
            this.w += 6;
        
        this.x = Math.floor(X - this.w * 0.5);
        this.y = Math.floor(Y - this.charHeight);
        this.timer = 1;
        this.speedy = -1;
        this.accely = 0.25;
        
        this.active = true;
        this.visible = true;
    }
    
    this.getWidth = function(SomeNumber, ZeroFilled, Length)
    {
        var xx = 0;
        var aNumber = 0;
        var zeroFilled = true;
        while (Length > 0)
        {
            if (SomeNumber > 0 || zeroFilled)
            {
                aNumber = SomeNumber % 10;
                SomeNumber = Math.floor(SomeNumber / 10);
                if (aNumber != 1)
                    xx += (this.charWidth - 1);
                else
                    xx += 2;
                zeroFilled = ZeroFilled;
            }
            Length--;
        }
        return xx;
    }
    
    this.write = function(Context, SomeNumber, X, Y, ZeroFilled, Length)
    {
        var xx = X - this.charWidth;
        var aNumber = 0;
        var zeroFilled = true;
        while (Length > 0)
        {
            if (SomeNumber > 0 || zeroFilled)
            {
                aNumber = SomeNumber % 10;
                Context.drawImage(this.fontImage, this.charWidth * aNumber, 0, this.charWidth, this.charHeight, xx, Y, this.charWidth, this.charHeight);
                SomeNumber = Math.floor(SomeNumber / 10);
                if (aNumber != 1)
                    xx -= (this.charWidth - 1);
                else
                    xx -= 2;
                zeroFilled = ZeroFilled;
            }
            Length--;
        }
    }
    
    this.update = function(Elapsed)
    {
        if (this.active)
        {
            if (this.timer > 0)
            {
                this.timer -= Elapsed;
                if (this.speedy < 1)
                {
                    this.y += this.speedy;
                    this.speedy += this.accely;
                }
                
                if (this.timer < 0)
                {
                    this.active = false;
                    this.visible = false;
                }
            }
        }
    }
    
    this.render = function(Context, OffsetX, OffsetY)
    {
        if (this.visible)
        {
            var xx = Math.floor(this.x + OffsetX);
            var yy = Math.floor(this.y + OffsetY);
            
            if (this.needPlusSign)
                Context.drawImage(this.fontImage, 60, 0, 6, 7, xx, yy, 6, 7);
            
            this.write(Context, this.score, xx + this.w, yy, false, this.numberOfChars);
        }
    }
}

function HeartBeat()
{
    this.angle = 0;
    this.delta = 0;
    
    this.update = function(Elapsed)
    {
        this.angle += 12;
        if (this.angle >= 360)
            this.angle -= 360;
        this.delta = m_math.sin(this.angle);
    }
}

var EFFECT_FADE_IN = 0;
var EFFECT_FADE_OUT = 1;

function dot_Effect()
{
    this.finished = true;
    this.fadeFunction = EFFECT_FADE_IN;
    this.duration = 0;
    this.info = 0;
    this.timer = 0;
    this.alpha = 0;
    this.tween = new dot_Tween;
    
    this.reset = function(FadeFunction, Duration, Info)
    {
        this.fadeFunction = FadeFunction;
        this.duration = Duration;
        this.info = Info;
        
        if (FadeFunction == EFFECT_FADE_IN)
        {
            this.alpha = 0;
            this.tween.reset(0, 1, EASE_NONE, Duration);
        }
        else
        {
            this.alpha = 1;
            this.tween.reset(1, 0, EASE_NONE, Duration);
        }
        
        this.finished = false;
    }
    
    this.update = function(Elapsed)
    {
        if (!this.tween.finished)
        {
            this.alpha = this.tween.delta;
        
            m_context.globalAlpha = this.alpha;
            m_context.fillStyle = "#000000";
            m_context.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            m_context.globalAlpha = 1;
            
            this.tween.update(Elapsed);
            
            if (this.tween.finished)
            {
                this.finished = true;
            }
        }
    }
}

function dot_ObjectPool()
{
    this.array = null;
    this.needUpdateReverse = false;
    
    this.create = function(Object, Population)
    {
        this.array = new Array(Population);
        
        for (var i = 0; i < Population; i++)
            this.array[i] = new Object();
        
        this.index = 0;
    }
    
    this.next = function()
    {
        var obj =  this.array[this.index];
        this.index++;
        if (this.index >= this.array.length)
            this.index = 0;
        return obj;
    }
    
    this.update = function(Elapsed)
    {
        if (!this.needUpdateReverse)
        {
            for (var i = 0; i < this.array.length; i++)
            {
                if (this.array[i].active)
                    this.array[i].update(Elapsed);
            }
        }
        else
        {
            for (var i = this.array.length - 1; i >= 0; i--)
            {
                if (this.array[i].active)
                    this.array[i].update(Elapsed);
            }
        }
    }
    
    this.render = function(Context, OffsetX, OffsetY)
    {
        if (!this.needUpdateReverse)
        {
            for (var i = 0; i < this.array.length; i++)
            {
                if (this.array[i].visible)
                    this.array[i].render(Context, OffsetX, OffsetY);
            }
        }
        else
        {
            for (var i = this.array.length - 1; i >= 0; i--)
            {
                if (this.array[i].visible)
                    this.array[i].render(Context, OffsetX, OffsetY);
            }
        }

    }
}

function dot_Button()
{
    this.image = null;
    this.create = function(Image, OffsetX, OffsetY, Width, Height)
    {
        this.image = Image;
        this.offsetX = OffsetX;
        this.offsetY = OffsetY;
        this.w = Width;
        this.h = Height;
        this.currentFrame = 0;
    }
    
    this.reset = function(X, Y)
    {
        this.x = Math.floor(X);
        this.y = Math.floor(Y);
        
        this.pressing = false;
        
        this.justReleased = false;
        this.justPressed = false;
        this.isPressed = false;
        this.currentFrame = 0;
        
        this.release();
        
        this.active = true;
    }
    
    this.press = function()
    {
        this.justReleased = false;
        if (!this.isPressed)
            this.justPressed = true;
        
        this.isPressed = true;
    }
    
    this.release = function()
    {
        this.justPressed = false;
        if (this.isPressed)
            this.justReleased = true;
        
        this.isPressed = false;
    }
        
    this.update = function(Elapsed)
    {
        this.justPressed = false;
        this.justReleased = false;
        
        if (this.pressing != this.isPressed)
        {
            if (this.isPressed)
                this.release();
            else
                this.press();
        }
    }
    
    this.render = function(Context, OffsetX, OffsetY)
    {
        if (this.visible)
        {
            var xx = Math.round(this.x + OffsetX);
            var yy = Math.round(this.y + OffsetY);
            
            if (this.isPressed)
                yy += 1;

            Context.drawImage(this.image, this.offsetX, this.offsetY, this.w, this.h, xx, yy, this.w, this.h);
        }
    }
}

var MEDAL_BRONZE = 0;
var MEDAL_SILVE = 1;
var MEDAL_GOLD = 2;
var MEDAL_PLATINUM = 3;

function dot_Blink()
{
    this.load(img_blink, 5, 5, 2, 2, 5, 5);
    this.blink = this.createAnimation("blink", 10, false, [0, 1, 2, 1, 0]);
    
    this.reset = function(X, Y)
    {
        this.x = Math.floor(X);
        this.y = Math.floor(Y);
        this.blink.play(true);
        this.active = true;
        this.visible = true;
    }
    
    this.update = function(Elapsed)
    {
        this.updateAnimation(Elapsed);
        
        if (this.currentAnim.finished)
        {
            this.active = false;
            this.visible = false;
        }
    }
}

function dot_Medal()
{
    m_blinkPool = new dot_ObjectPool();
    m_blinkPool.create(dot_Blink, 4);
    this.timer = 0.1;
    this.x = 0;
    this.y = 0;
    this.w = 0;
    this.h = 0;
    this.active = false;
    this.visible = false;
    this.medalType = 0;
    
    this.reset = function(X, Y, MedalType)
    {
        this.x = Math.floor(X);
        this.y = Math.floor(Y);
        this.w = 15;
        this.h = 24;
        this.medalType = MedalType;
     
        this.active = true;
        this.visible =true;
    }
    
    this.update = function(Elapsed)
    {
        m_blinkPool.update(Elapsed);
        
        if (this.timer > 0)
        {
            this.timer -= Elapsed;
            if (this.timer <= 0)
            {
                this.timer = 0.5;
                m_blinkPool.next().reset(this.x - 3 + Math.random() * (this.w + 6), this.y - 3 + Math.random() * (this.h + 6));
            }
        }
    }
    
    this.render = function(Context, OffsetX, OffsetY)
    {
        Context.drawImage(img_medals, 15 * this.medalType, 0, 15, 24, this.x, this.y, 15, 24);
        m_blinkPool.render(Context, OffsetX, OffsetY);
    }
}

function dot_Time()
{
    this.start_time = new Array(10);
    this.duration = new Array(10);
    this.func = new Array(10);
    this.index = 0;
    
    this.addTimeOut = function(TimeOut, Function)
    {
        this.start_time[this.index] = m_curTime;
        this.duration[this.index] = TimeOut * 1000;
        this.func[this.index] = Function;
        this.index++;
        if (this.index == 10)
            this.index = 0;
    }
    
    this.update = function(Elapsed)
    {
        for (var i = 0; i < 10; i++)
        {
            if (this.duration[i] > 0)
            {
                if (m_curTime - this.start_time[i] > this.duration[i])
                {
                    this.func[i]();
                    this.duration[i] = 0;
                }
            }
        }
    }
}

function dot_ScorePanel()
{
    this.image = null;
    this.active = false;
    this.visible = false;
    this.x = 0;
    this.y = 0;
    this.tween = new dot_Tween();
    this.alpha = 0;
    this.tweenAlpha = new dot_Tween();
    this.state = 0;
    this.needNew = false;
    this.medal = new dot_Medal();
    this.scoreFont = null;
    this.score = 0;
    this.best = 0;
    this.curScore = 0;
    this.storageName = null;
    this.w = SCREEN_WIDTH;
    this.h = 0;
    this.mileStone0 = 40;
    this.mileStone1 = 60;
    this.mileStone2 = 80;
    this.mileStone3 = 100;
    
    this.create = function(Image, LocalStorageName, ScoreFont, MileStone0, MileStone1, MileStone2, MileStone3)
    {
        this.image = Image;
        this.scoreFont = ScoreFont;
        this.storageName = LocalStorageName;
        this.h = Image.height;
        this.mileStone0 = MileStone0;
        this.mileStone1 = MileStone1;
        this.mileStone2 = MileStone2;
        this.mileStone3 = MileStone3;
    }
    
    this.reset = function(Score)
    {
        this.active = true;
        this.visible = true;
        this.x = (SCREEN_WIDTH - this.image.width) >> 1;
        this.y = SCREEN_HEIGHT;
        this.tween.reset(this.y, (SCREEN_HEIGHT - this.image.height) >> 1, EASE_QUAD_INOUT, 0.5);
        this.tweenAlpha.reset(0, 1, EASE_QUAD_INOUT, 0.5);
        this.needNew = false;
        this.medal.active = false;
        this.medal.visible = false;
        try {
            if (localStorage && localStorage.getItem(this.storageName))
                this.best = localStorage.getItem(this.storageName);
            else
                this.best = 0;
        } catch (e)
        {
            this.best = 0;
        }
        this.score = 0;
        this.curScore = Score;
        this.state = 0;
    }
    
    this.update = function(Elapsed)
    {
        switch (this.state)
        {
            case 0:
            {
                this.tween.update(Elapsed);
                this.tweenAlpha.update(Elapsed);
                this.alpha = this.tweenAlpha.delta;
                this.y = this.tween.delta;
                if (this.tween.finished)
                {
                    this.state = 1;
                    if (this.curScore > 0)
                        this.tween.reset(0, this.curScore, EASE_QUAD_INOUT, 1);
                    else
                        this.tween.reset(0, this.curScore, EASE_QUAD_INOUT, 0.1);
                    this.score = 0;
                }
                break;
            }
                
            case 1:
            {
                this.tween.update(Elapsed);
                this.alpha = 1;
                this.score = Math.round(this.tween.delta);
                if (this.tween.finished)
                {
                    if (this.score > this.best)
                    {
                        this.best = this.score;
                        this.needNew = true;
                        try {
                            if (localStorage)
                                localStorage.setItem(this.storageName, this.best);
                        } catch (e)
                        {
                        }
                    }
                    
                    if (this.score >= this.mileStone3)
                        this.medal.reset(this.x + 17, this.y + 20, 0);
                    else if (this.score >= this.mileStone2)
                        this.medal.reset(this.x + 17, this.y + 20, 1);
                    else if (this.score >= this.mileStone1)
                        this.medal.reset(this.x + 17, this.y + 20, 2);
                    else if (this.score >= this.mileStone0)
                        this.medal.reset(this.x + 17, this.y + 20, 3);
                    this.state = 2;
                }
                break;
            }
                
            case 2:
            {
                if (this.medal.active)
                    this.medal.update(Elapsed);
                break;
            }
        }
    }
    
    this.render = function(Context, OffsetX, OffsetY)
    {
        Context.globalAlpha = this.alpha;
        var xx = Math.floor(this.x);
        var yy = Math.floor(this.y);
        Context.drawImage(this.image, xx, yy);
        this.scoreFont.write(Context, this.score, xx + 102, yy + 16, false, 8);
        this.scoreFont.write(Context, this.best, xx + 102, yy + 37, false, 8);
        
        if (this.needNew)
            Context.drawImage(img_new, xx + 60, yy + 29);
        if (this.medal.visible)
            this.medal.render(Context, OffsetX, OffsetY);

        Context.globalAlpha = 1;
    }
}

this.updatePreloader = function()
{
    if (m_resourceCounter <= m_resourceNumber && m_loaderRenderEnabled == 2)
    {
        m_context.fillStyle = BACKGROUND_COLOR;
        m_context.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        var xx = Math.floor((SCREEN_WIDTH - img_load_bar.width) / 2);
        var yy = Math.floor((SCREEN_HEIGHT - img_load_bar.height) * 0.8);
        
        //m_context.drawImage(img_brand_logo, Math.floor((SCREEN_WIDTH - img_brand_logo.width) * 0.5), Math.floor(yy * 0.5));
        m_context.drawImage(img_load_bar, xx, yy);
        
        var pec = Math.floor((m_resourceCounter *  5) / m_resourceNumber);
        xx += this.img_load_element.width;
        for (var i = 0; i < pec; i++)
        {
            xx += img_load_element.width * 2;
            m_context.drawImage(img_load_element, xx, yy + img_load_element.width * 3);
        }
        
        if (m_resourceCounter == m_resourceNumber)
        {
            appInit();
            m_resourceCounter++;
        }
    }
}

this.gameInit = function()
{
    //overide this
}

this.gameUpdate = function(Elapsed)
{
    //overide this
}

this.touchPressed = function(X, Y)
{
    //override this
}

this.touchReleased = function(X, Y)
{
    //override this
}

this.backgroundUpdate = function(Context)
{
    //override this
}

var m_screen_width;
var m_screen_height;

this.screenResize = function()
{
	setTimeout("window.scrollTo(0, 1)", 0);
	
    m_screenRenew = true;
    
    if (window.innerWidth > window.innerHeight) //prevent landscape mode
    {
        m_paused = true;
        var _topCanvas = document.getElementById("portrait");
        _topCanvas.style.display = "block";
        if (m_canvas)
            m_canvas.style.display = "none";
    }
    else
    {
                
        m_paused = false;
        var _topCanvas = document.getElementById("portrait");
        _topCanvas.style.display = "none";
        
        document.body.style.height = (window.innerHeight + 50) + 'px';
        setTimeout( function(){ window.scrollTo(0, 1); }, 50);
        
        if (!m_canvas)
        {
            m_canvas = document.createElement('canvas');
            m_canvas.setAttribute('id', 'game_canvas');
            m_canvas.setAttribute('width', '' + SCREEN_WIDTH);
            m_canvas.setAttribute('height', '' + SCREEN_HEIGHT);
            document.body.appendChild(m_canvas);
        }
        
        var _scale = Math.min(window.innerWidth / 144, window.innerHeight / 208);
        m_screen_width = Math.round(144 * _scale);
        m_screen_height = Math.round(208 * _scale);
        m_canvas.setAttribute('style', 'width:' + m_screen_width + 'px;height:' + m_screen_height + 'px;position:fixed;left:50%;top:50%;margin-top:-' + Math.round(m_screen_height * 0.5) + 'px;margin-left:-' + Math.round(m_screen_width * 0.5) + 'px;z-index:1;');
        
        m_context = m_canvas.getContext('2d');
        
        m_canvas.style.display = "block";
    }
}

this.update = function()
{
    m_curTime = new Date().getTime();
    m_elapsed = (m_curTime - m_lastTime);
    
    if (m_elapsed > 30 && !m_paused)
    {
        m_elapsed = 0.03;
        m_lastTime = m_curTime;
        
        m_heartBeat.update(m_elapsed);
        m_scoreContextPool.update(m_elapsed);
        
        m_time.update(m_elapsed);
        
        gameUpdate(m_elapsed, m_context);
        
        m_scoreContextPool.render(m_context, 0, 0);
        
        m_effect.update(m_elapsed);
    }
    
    if (m_isReqAnimationEnabled)
        _reqAnimation(update);

}

this.appInit = function()
{
    dot_ScoreContext.prototype = new dot_Sprite();
    dot_ScoreContext.prototype.constructor = dot_ScoreContext;
    
    dot_Button.prototype = new dot_Sprite();
    dot_Button.prototype.constructor = dot_Button;
    
    dot_Blink.prototype = new dot_Sprite();
    dot_Blink.prototype.constructor = dot_Blink;
    
    m_ignoreTouchMove = false;
    
    m_math = new dot_Math();
    m_math.load();
    
    m_heartBeat = new HeartBeat();
    
    m_effect = new dot_Effect();
    
    m_scoreContextPool = new dot_ObjectPool();
    m_scoreContextPool.create(dot_ScoreContext, 10);
    
    m_time = new dot_Time();
    
    gameInit();
    
    m_effect.reset(EFFECT_FADE_OUT, 0.5, 0);
    
    //update initializing
    _reqAnimation = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame;
    
    if(_reqAnimation)
    {
        m_isReqAnimationEnabled = true;
        update();
    }
    else
    {
        m_isReqAnimationEnabled = false;
        setInterval(update, 33);
    }
    
//    console.log("Game Initialized Successful.");
}

this.resourceLoad = function()
{
    if (m_paused)
    {
        setTimeout(this.resourceLoad, 1);
    }
    else
    {
        if (m_resourceCounter == 0)
        {
            var self = this;
            
            m_resourceNumber = m_imageList.length;
            m_resourceCounter = 0;
            
            m_lastTime = new Date().getTime();
            
            for (var i = 0; i < m_imageList.length; )
            {
                m_curTime = new Date().getTime();
                m_elapsed = (m_curTime - m_lastTime);
                
                if (m_elapsed > 30)
                {
                    var _img = new Image();
                    if (i < 2)
                    {
                        _img.onload = function()
                        {
                            m_resourceCounter++;
                            m_loaderRenderEnabled++;
                            updatePreloader();
                        };
                    }
                    else
                    {
                        _img.onload = function()
                        {
                            m_resourceCounter++;
                            updatePreloader();
                        };
                    }
                    
                    _img.src = SG_ImagePath + m_imageList[i] + ".png";
                    this["img_" + m_imageList[i]] = _img;
                    m_lastTime = new Date().getTime();
                    i++;
                }
            }
        }
    }
}

var m_isMouseDown = false;

this.globalInit = function()
{
    if (navigator.userAgent.match(/MSIE/)) //suport IE9 Mango
    {
        if (!window.console)
        {
            var console = {};
        }
        if (!console.log)
        {
            console.log = function() {};
        }
    }
    
    m_paused = true;
    
    SCREEN_WIDTH = 144;
    SCREEN_HEIGHT = 208;
    
    document.body.addEventListener('touchmove',function(e)
                                   {
                                   if (!m_ignoreTouchMove)
                                   {
                                   if (e.changedTouches.length > 0)
                                   {
                                   m_touch_x = Math.floor(SCREEN_WIDTH * (e.changedTouches[0].pageX - m_canvas.offsetLeft) / m_screen_width);
                                   m_touch_y = Math.floor(SCREEN_HEIGHT * (e.changedTouches[0].pageY - m_canvas.offsetTop) / m_screen_height);
                                   touchPressed(m_touch_x, m_touch_y);
                                   }
                                   }
                                   e.preventDefault();
                                   });
    
    document.body.addEventListener('touchstart',function(e)
                                   {
                                   for (var i = 0; i < e.targetTouches.length; i++)
                                   {
                                       m_touch_x = Math.floor(SCREEN_WIDTH * (e.targetTouches[i].pageX - m_canvas.offsetLeft) / m_screen_width);
                                       m_touch_y = Math.floor(SCREEN_HEIGHT * (e.targetTouches[i].pageY - m_canvas.offsetTop) / m_screen_height);
                                       touchPressed(m_touch_x, m_touch_y);
                                   }
                                   e.preventDefault();
                                   });
    
    document.body.addEventListener('touchend',function(e)
                                   {
                                   for (var i = 0; i < e.changedTouches.length; i++)
                                   {
                                       m_touch_x = Math.floor(SCREEN_WIDTH * (e.changedTouches[i].pageX - m_canvas.offsetLeft) / m_screen_width);
                                       m_touch_y = Math.floor(SCREEN_HEIGHT * (e.changedTouches[i].pageY - m_canvas.offsetTop) / m_screen_height);
                                       touchReleased(m_touch_x, m_touch_y);
                                       m_touch_x = -1000;
                                       m_touch_y = -1000;
                                   }
                                   e.preventDefault();
                                   });
    
    document.body.addEventListener('gesturestart',function(e){
                                   e.preventDefault();
                                   });
    
    document.body.addEventListener('gesturechange',function(e){
                                   e.preventDefault();
                                   });
    
    document.body.addEventListener('gestureend',function(e){
                                   e.preventDefault();
                                   });
    
    document.body.addEventListener('mouseup', function(e)
                                   {
                                   m_isMouseDown = false;
                                   m_touch_x = Math.floor(SCREEN_WIDTH * (e.pageX - m_canvas.offsetLeft) / m_screen_width);
                                   m_touch_y = Math.floor(SCREEN_HEIGHT * (e.pageY - m_canvas.offsetTop) / m_screen_height);
                                   touchReleased(m_touch_x, m_touch_y);
                                   m_touch_x = -1000;
                                   m_touch_y = -1000;
                                   e.preventDefault();
                                   });
    
    document.body.addEventListener('mousedown', function(e)
                                   {
                                   m_isMouseDown = true;
                                   m_touch_x = Math.floor(SCREEN_WIDTH * (e.pageX - m_canvas.offsetLeft) / m_screen_width);
                                   m_touch_y = Math.floor(SCREEN_HEIGHT * (e.pageY - m_canvas.offsetTop) / m_screen_height);
                                   touchPressed(m_touch_x, m_touch_y);
                                   e.preventDefault();
                                   });
    document.body.addEventListener('mousemove', function(e)
                                   {
                                   if (!m_ignoreTouchMove)
                                   {
                                   if (m_isMouseDown)
                                   {
                                   m_touch_x = Math.floor(SCREEN_WIDTH * (e.pageX - m_canvas.offsetLeft) / m_screen_width);
                                   m_touch_y = Math.floor(SCREEN_HEIGHT * (e.pageY - m_canvas.offsetTop) / m_screen_height);
                                   touchPressed(m_touch_x, m_touch_y);
                                   }
                                   }
                                   e.preventDefault();
                                   
                                   });
    
    window.addEventListener('orientationchange', function(){
                            screenResize();
                            });
    
    window.addEventListener('resize', function() {
                            screenResize();
                            });
    

    softgames.changeScreenOrientation = screenResize;
    softgames.changeScreenSize = screenResize;
    
    screenResize();
    
    setTimeout(this.resourceLoad, 1000);
}
